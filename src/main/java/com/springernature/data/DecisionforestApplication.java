package com.springernature.data;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DecisionforestApplication {

    public static void main(String[] args) {
        SpringApplication.run(DecisionforestApplication.class, args);
    }
}
